#!/usr/bin/env sh

set -eux

sudo dnf install --assumeyes ansible git

mkdir -p ~/projects
git clone https://gitlab.com/BenjaminSchubert/infrastructure.git ~/projects/infrastructure

cd ~/projects/infrastructure

ansible-playbook --ask-become-pass provision.yml
